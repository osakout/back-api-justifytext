const winston = require('winston');

winston.loggers.add('logger', {
  console: {
    level: 'debug',
    colorize: true,
  },
});

const logger = winston.loggers.get('logger');
// logger.error('\x1b[36m%s\x1b[0m', 'I am cyan');
module.exports = logger;
