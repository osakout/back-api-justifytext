const mongoose = require('mongoose');
// const uuidv1 = require('uuid/v1');

const { Schema } = mongoose;

/**
 * User schema
 */

const UserSchema = new Schema({
  email: String,
});

/**
 * Methods
 */

UserSchema.method({

});

/**
 * Statics
 */

UserSchema.static({

});

module.exports = mongoose.model('User', UserSchema);
