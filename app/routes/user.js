const express = require('express');
var jwt = require('jsonwebtoken');
const Status = require('../helpers/api-status');
const { respond } = require('../utils/index');

const userAdapter = require('../v1/user/userAdapter');

const router = express.Router();

/* User landing. */
router.get('/', (req, res) => {
  respond(res, { data: {}, meta: Status.WELCOME },
    Status.WELCOME.status);
});

router.post('/token', (req, res) => {
  const { email } = req.body;

  if (!email) {
    respond(res, { data: {}, meta: Status.UNAUTHORIZED_MAIL },
      Status.UNAUTHORIZED_MAIL.status);
  }

  userAdapter.register(email).then((payload) => {

    var token = jwt.sign({ email: payload.email }, process.env.SECRET_TOKEN , {
      expiresIn: 86400 // expires in 24 hours
    });
    respond(res, { data: { email: payload.email, token: token }, meta: Status.CREATED },
      Status.CREATED.status);

  }).catch(() => {

    respond(res, { data: {}, meta: Status.INTERNAL_SERVER_ERROR },
      Status.INTERNAL_SERVER_ERROR.status);

  });
});

router.post('/justify', (req, res) => {
  const {authorization, email} = req.headers;

  if (!authorization) return respond(res, { data: {}, meta: Status.UNAUTHORIZED },
    Status.UNAUTHORIZED.status);
  
  jwt.verify(authorization, process.env.SECRET_TOKEN, function(err, decoded) {
    if (err) return respond(res, { data: {}, meta: Status.FAILED_TOKEN }, Status.FAILED_TOKEN.status);

    userAdapter.login(email).then(payload => {

      if (payload.email === decoded.email) {
        const textJustified = userAdapter.justifyText(req.body);
        res.setHeader('Content-Type', 'text/plain');
        res.status(200).send(textJustified);
      } else {
        respond(res, { data: {}, meta: Status.FORBIDDEN },
          Status.FORBIDDEN.status);
      }

    }).catch(error => {
      respond(res, { data: {}, meta: Status.FORBIDDEN },
        Status.FORBIDDEN.status);
    })
  });
  
});

module.exports = router;
