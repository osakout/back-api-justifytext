const bcrypt = require('bcryptjs');
const sha256 = require('sha256');
const userRepository = require('./userRepository');
const logger = require('../../log/logger');

class UserAdapter {

  async register(email) {
    try {
      const user = await userRepository.createUser({ email });
      logger.info(`User successfully registered in register() [userAdapter.js]\n${user}`);
      return user;
    } catch (error) {
      logger.error(`Error in register() [userAdapter.js] ${error}`);
      return error;
    }
  }

  chiffrement(mdp) {
    const salt = bcrypt.genSaltSync(10);
    const chiffrer = bcrypt.hash(sha256(mdp), salt);

    return chiffrer;
  }

  async login(email) {
    try {
      const user = await userRepository.findOneUser(email);
      console.log(email);
      return user;
    } catch (error) {
      logger.error(`Error in login() [userAdapter.js] ${error}`);
      return error;
    }
  }


  distributeSpaces(str, n) {
    let i = 0;
    let myArray = str.split(' ');
    myArray = myArray.map(x => x = `${x} `);
    let lengthArray = myArray.length;

    for (var x = 0; x < n; x++) {
      if (x < lengthArray) {
        myArray[i] = `${myArray[i]} `;
        i++;
      } else {
        i = 0;
      }
    }
    return myArray.join('');
  }

  justifyText(text) {
    let textArray = text.split(' ');
    let i = 0;
    let j = 0;
    let nbLigne = 0;
    let ligneArray = [];
    let arrayJustified = [];
    let indexPrecedent = [];
    let spaces = 0;

    textArray = textArray.map(x => x = `${x} `);
    let lastItem = textArray.length - 1;

    textArray.forEach(element => {

      if (nbLigne + element.length < 80) {
        nbLigne = element.length + nbLigne;
      } else {
        indexPrecedent[0] = element;
        spaces = 80 - nbLigne;
        while (j < i) {
          ligneArray.push(textArray[j]);
          j++;
        }
        let text = this.distributeSpaces(ligneArray.join(''), spaces);
        arrayJustified.push(text);
        j = i;
        nbLigne = indexPrecedent[0].length;
        ligneArray = [];
      }
      if (i === lastItem) {
        while (j <= i) {
          ligneArray.push(textArray[j]);
          j++;
        }
        arrayJustified.push(ligneArray.join(''));
      }
      i++;
    });

    return arrayJustified.join('\n');
  }

}

const userAdapter = new UserAdapter();

module.exports = userAdapter;
