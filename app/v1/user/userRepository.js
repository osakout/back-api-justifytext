const User = require('../../models/user');
const logger = require('../../log/logger');

class UserRepository {
  async createUser(userData) {
    try {
      console.log(userData);
      const user = await User.create(userData);
      logger.info(`User successfully created in CreateUser() [userRepository.js]\n${JSON.stringify(user)}`);
      return user;
    } catch (error) {
      logger.error(`Error while creating User in createUser() [userRepository.js]\n${error}`);
      throw error;
    }
  }

  async findOneUser(userData) {
    try {
      const user = await User.findOne({ email: userData });
      logger.info(`User successfully find in findOneUser() [userRepository.js]\n${JSON.stringify(user)}`);
      return user;
    } catch (error) {
      logger.error(`Error while finding User in findOneUser() [userRepository.js]\n${error}`);
      throw error;
    }
  }
}

const userRepository = new UserRepository();

module.exports = userRepository;
