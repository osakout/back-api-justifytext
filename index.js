require('./config/config.js');

const express = require('express');
const validator = require('express-validator');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const winston = require('winston');
const cors = require('cors');
const logger = require('./app/log/logger');
const landing = require('./app/routes');
const users = require('./app/routes/user');

const app = express();

// connect to Mongo when the app initializes
mongoose.connect(process.env.DB_PATH).then(
  () => { logger.info('Database successfully connected !'); },
  (err) => { logger.error(`Error Connecting Database !\n${err}`); },
);

/* db.once('open', function() {
  console.log("we successfully connect to DB");
  // we're connected!
}); */

// Set CORS(Cross-origin resource sharing) options
const corsOptions = {
  origin: '*',
  allowedHeaders: ['Content-Type', 'Authorization'],
  optionsSuccessStatus: 200,
};

// Set logging level
winston.level = process.env.LOG_LEVEL;

app.use(bodyParser.json());

app.use(bodyParser.text());

app.use(validator({
  customValidators: {
  },
  customSanitizers: {
  },
}));

app.use(cors(corsOptions));

app.use('/', landing);
app.use('/api', users);

process.on('uncaughtException', (err) => {
  logger.info('crit', err.stack);
});

app.listen(process.env.PORT || process.env.LISTENER_PORT, () => {
  logger.info(`App is listening on http://localhost:${process.env.PORT || process.env.LISTENER_PORT}/`);
});
