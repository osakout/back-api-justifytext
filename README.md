API JUSTIFY TEXT
===========================

Please see Trello : https://trello.com/b/w4jsLwtB/back-api-justifytext

App is deployed here => https://secret-ridge-77922.herokuapp.com/

Endpoints
======================

***[POST] /api/token***
-------------

***Content-Type*** : ```
application/json```

***Body*** : 
```
{
    "email": "yourEmail@mail.fr"
}
```

***Response*** :

```
{
    "data": {
        "email": "yourEmail@mail.fr",
        "token": "YourToken"
    },
    "meta": {
        "code": "CREATED",
        "status": 201,
        "title": "Created",
        "detail": "Successfully created"
    }
}
```

***[POST] /api/justify***
-------------

***Content-Type*** : ```text/plain```

***Headers*** :
```
{
    "Authorization" : "YourToken",
    "email" : "yourEmail@mail.fr"
}
```

***Body*** : 

```
Iamque non umbratis fallaciis res agebatur, sed qua palatium est extra muros, armatis omne circumdedit. ingressusque obscuro iam die, ablatis regiis indumentis Caesarem tunica texit et paludamento communi, eum post haec nihil passurum velut mandato principis iurandi crebritate confirmans et statim inquit exsurge et inopinum carpento privato inpositum ad Histriam duxit prope oppidum Polam, ubi quondam peremptum Constantini filium accepimus Crispum.
```

***Response*** :

```
Iamque  non  umbratis fallaciis res agebatur, sed qua palatium est extra muros,  
armatis  omne  circumdedit.  ingressusque  obscuro  iam  die,  ablatis  regiis   
indumentis  Caesarem  tunica  texit et paludamento communi, eum post haec nihil  
passurum  velut  mandato  principis  iurandi  crebritate  confirmans  et statim  
inquit  exsurge  et inopinum carpento privato inpositum ad Histriam duxit prope  
oppidum Polam, ubi quondam peremptum Constantini filium accepimus Crispum. 
```

***Error*** :

***Email given doesn't match the token created before by the email in /api/token***
```
{
    "data": {},
    "meta": {
        "code": "FORBIDDEN",
        "status": 403,
        "title": "User",
        "detail": "Wrong user"
    }
}
```


***Token given doesn't exist***
```
{
    "data": {},
    "meta": {
        "code": "FAILED_TOKEN",
        "status": 500,
        "title": "Token",
        "detail": "Failed to authenticate token."
    }
}
```


***Token undefined***
```
{
    "data": {},
    "meta": {
        "code": "UNAUTHORIZED",
        "status": 401,
        "title": "Token",
        "detail": "No token provided."
    }
}
```